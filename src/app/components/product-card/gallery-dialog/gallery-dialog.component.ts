import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-gallery-dialog',
  templateUrl: './gallery-dialog.component.html',
  styleUrls: ['./gallery-dialog.component.css']
})
export class GalleryDialogComponent implements OnInit {
  currentImg: any;
  allImages: any;

  constructor(public dialogRef: MatDialogRef<GalleryDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.allImages = [
      ['../../../../assets/img/stubProduct.gif', '../../../../assets/img/largeProduct.jpg'],
      ['../../../../assets/img/stubProduct.gif', '../../../../assets/img/largeProduct.jpg'],
      ['../../../../assets/img/stubProduct.gif', '../../../../assets/img/largeProduct.jpg'],
      ['../../../../assets/img/largeProduct.jpg', '../../../../assets/img/stubProduct.gif'],
    ];

    this.currentImg = this.data.images;
  }

  ngOnInit() {
  }

  // changing image for shown in large format
  showImg(image) {
    this.currentImg = image;
  }

}
