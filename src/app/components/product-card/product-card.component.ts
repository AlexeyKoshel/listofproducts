import {Component, Input, OnInit} from '@angular/core';
import {EventsService} from '../../services/events.service';
import {SimpleStoreService} from '../../services/simple-store.service';
import {MatDialog} from '@angular/material';
import {GalleryDialogComponent} from './gallery-dialog/gallery-dialog.component';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  check: boolean;
  images: Array<string>;

  @Input() item: any;

  constructor(private events: EventsService,
              private store: SimpleStoreService,
              public dialog: MatDialog) {
    this.check = false;
    this.images = [
      '../../../../assets/img/stubProduct.gif',
      '../../../../assets/img/largeProduct.jpg'
    ];
  }

  ngOnInit() {
    // checking is the product added to watchlist
    this.store.watchlistArray.forEach((watchItem) => {
      if (watchItem.data.id_product === this.item.data.id_product) {
        this.check = true;
      }
    });
  }

  // add or remove product from the watchlist
  action() {
    this.events.watchlistItemEvent.next({adding: this.check, item: this.item});
  }

  // open gallery as mat-dialog
  openDialog(): void {
    const dialogRef = this.dialog.open(GalleryDialogComponent, {
      data: { name: `${this.item.data.systemtitle} / ${this.item.data.systemnumber}`, images: this.images}
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
