import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/operators/startWith';
import 'rxjs/operators/map';
import {map, startWith} from 'rxjs/operators';
import {EventsService} from '../../services/events.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  myControl: FormControl = new FormControl();

  options = [];

  filteredOptions: Observable<string[]>;

  dampingstrengthRange: {
    start: any,
    finish: any,
  };

  filters: {
    name: string,
    fireresistance: Array<any>,
    soundproof_filter: any,
    wallthickness: any,
    dampingstrength: any,
    docType: any,
  };

  constructor(private events: EventsService) {
    this.filters = {
      name: '',
      fireresistance: [],
      soundproof_filter: {
        all: true,
        value: 0,
      },
      wallthickness: {
        all: true,
        value: 0,
      },
      dampingstrength: {
        all: true,
        value: 0,
      },
      docType: 'Datasheets',
    };
    this. dampingstrengthRange = {
      start: 1,
      finish: 300,
    };
    this.events.nameSpaceEvent.subscribe((names) => {
      this.options = names;
    });
    this.events.dampingstrengthRangeEvent.subscribe((range) => {
      this.dampingstrengthRange.start = range[0];
      this.dampingstrengthRange.finish = range[1];
    });
    this.events.fireResistanceFiltersEvent.subscribe((values) => {
      this.filters.fireresistance = [];
      values.forEach((value) => {
        this.filters.fireresistance.push({name: value, value: false});
      });
    });
  }

  ngOnInit() {
    // Initial initialization of filters
    this.loadFilters();

    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filter(val))
      );
  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  // loading filters from the local storage
  loadFilters() {
    const cachedFilters = localStorage.getItem('filter-cache');
    if (cachedFilters) {
      this.filters = JSON.parse(cachedFilters);
      this.applyFilters();
    }
  }

  // applying selected filters to the products
  applyFilters() {
    localStorage.setItem('filter-cache', JSON.stringify(this.filters));
    this.events.filterParameterEvent.next(this.filters);
  }

  // reset all filters options, deleting from the local storage
  resetFilters() {
    this.filters = {
      name: '',
      fireresistance: [],
      soundproof_filter: {
        all: true,
        value: 0,
      },
      wallthickness: {
        all: true,
        value: 0,
      },
      dampingstrength: {
        all: true,
        value: 0,
      },
      docType: 'Datasheets',
    };
    this.events.filterParameterEvent.next('reset');
    localStorage.removeItem('filter-cache');
  }
}
