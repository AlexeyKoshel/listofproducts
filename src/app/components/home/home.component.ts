import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../../services/products.service';
import {SimpleStoreService} from '../../services/simple-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private productService: ProductsService, private store: SimpleStoreService) { }

  ngOnInit() {
    this.productService.getAllProducts();  // loading products data
  }

}
