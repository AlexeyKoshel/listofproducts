import { Component, OnInit } from '@angular/core';
import {EventsService} from '../../services/events.service';
import {SimpleStoreService} from '../../services/simple-store.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  shownProducts: Array<any>;
  filteredProducts: Array<any>;
  icons: any;
  view: string;
  scroll: boolean;
  loading: boolean;

  constructor(private events: EventsService,
              private store: SimpleStoreService) {
    this.shownProducts = [];
    this.scroll = false;
    this.loading = false;
    this.view = 'card';
    this.filteredProducts = this.store.filteredProducts;

    this.events.filteredProductsEvent.subscribe((products) => {
      this.filteredProducts = products;
      this.initShowArray();
    });

    this.icons = {
      cards: '../../../assets/img/cards.png',
      lists: '../../../assets/img/lists.png',
    };
  }

  ngOnInit() {
    // start load products
    this.initShowArray();
  }

  // display first ten products
  initShowArray() {
    this.shownProducts = [];
    this.loading = true;
    const obs = new Observable((observer) => {
      setTimeout(() => observer.next(this.filteredProducts.slice(0, 10)), 2000);
    }).subscribe((data: any) => {
      this.shownProducts = data;
      this.events.updateFiltersParametersEvent.next(this.shownProducts);
      this.loading = false;
    });
  }

  // add next five products
  addMore() {
    if (this.shownProducts.length < this.filteredProducts.length) {
      this.loading = true;
      const obs = new Observable((observer) => {
        setTimeout(() => observer.next(this.filteredProducts
          .slice(this.shownProducts.length, this.shownProducts.length + 5)), 2000);
      }).subscribe((data: any) => {
        data.map(item => {
          this.shownProducts.push(item);
          this.events.updateFiltersParametersEvent.next(this.shownProducts);
          this.loading = false;
        });
      });
    }
  }

  // event handler for scroll event
  onScroll() {
    if (this.scroll) {
      this.addMore();
    }
  }
}
