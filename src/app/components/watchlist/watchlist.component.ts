import { Component, OnInit } from '@angular/core';
import {EventsService} from '../../services/events.service';
import {SimpleStoreService} from '../../services/simple-store.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  watchlistArray: Array<any>;

  constructor(private events: EventsService, private store: SimpleStoreService) {
    this.watchlistArray = this.store.watchlistArray;

    this.events.watchlistArrayEvent.subscribe((nweWatchlistArray) => {
      this.watchlistArray = nweWatchlistArray;
    });
  }

  ngOnInit() {
  }

}
