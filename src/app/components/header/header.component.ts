import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  navLinks: Array<{path: string, tooltip: string, text: string}>;

  constructor(private router: Router) {
    // parameters for navigation
    this.navLinks = [
      {path: 'products', tooltip: 'products', text: 'products'},
      {path: 'watchlist', tooltip: 'watchlist', text: 'watchlist'},
    ];
  }

  ngOnInit() {
  }

}
