import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as data from './data.json';
import {EventsService} from './events.service';
import {SimpleStoreService} from './simple-store.service';

@Injectable()
export class ProductsService {
  stringData: string;
  parsedData: any;

  constructor(private events: EventsService, private store: SimpleStoreService) {
    this.stringData = JSON.stringify(data);
    this.parsedData = JSON.parse(this.stringData);

    this.events.filterParameterEvent.subscribe((filter) => {
      if (filter === 'reset') {
        this.getFireResistanceFilters(this.store.allProductsArray);
        this.events.filteredProductsEvent.next(this.store.allProductsArray);
      } else {
        this.filter(filter);
      }
    });
    this.events.updateFiltersParametersEvent.subscribe((arr) => {
      this.getNameSpace(arr);
      this.getDampingstrengthRange(arr);
    });

    this.events.watchlistItemEvent.subscribe((item) => {
      this.watchlistEditor(item);
    });
  }

  // initial getting data
  getAllProducts() {
    const parsedProductsArray = [];

    Object.keys(this.parsedData.product).forEach((item) => {
      const docType = ['Datasheets', 'Pricelists', ''];
      this.parsedData.product[item].docType = [docType[Math.floor(Math.random() * docType.length)]];
      parsedProductsArray.push(this.parsedData.product[item]);
    }, this.parsedData.product);
    this.getFireResistanceFilters(parsedProductsArray); // select all fire resistance values for filter
    this.events.allProductsArrayEvent.next(parsedProductsArray);
  }

  // application of a set of filters to data
  filter(filtersParam) {
    const newFilteredProducts: Array<any> = this.store.allProductsArray.filter((item, i, arr) => {
      const re = new RegExp(  filtersParam.name, 'i');
      return (
        (re.test(item.data.systemtitle) || re.test(item.data.systemnumber)) &&
        filtersParam.fireresistance.some((filter) => filter.name === item.data.fireresistance && filter.value) &&
        (filtersParam.soundproof_filter.all || (item.data.soundproof_filter === String(filtersParam.soundproof_filter.value))) &&
        (filtersParam.wallthickness.all || (item.data.wallthickness === String(filtersParam.wallthickness.value))) &&
        (filtersParam.dampingstrength.all || (item.data.dampingstrength === String(filtersParam.dampingstrength.value))) &&
        (item.docType && item.docType.some((filter) => (filter === filtersParam.docType)))
      );
    });

    this.getNameSpace(newFilteredProducts);
    this.getDampingstrengthRange(newFilteredProducts);
    this.events.filteredProductsEvent.next(newFilteredProducts);
  }

  // adding or deleting products from watchlist
  watchlistEditor(product) {
    const oldWatchlistArray = this.store.watchlistArray;
    const newWatchlistArray = [];

    if (product.adding) {
      oldWatchlistArray.push(product.item);
      this.events.watchlistArrayEvent.next(oldWatchlistArray);
    } else {
      oldWatchlistArray.forEach((item) => {
        if (item.data.id_product !== product.item.data.id_product) {
          newWatchlistArray.push(item);
        }
      });
      this.events.watchlistArrayEvent.next(newWatchlistArray);
    }
  }

  // selecting all the "system name" and "system number" from the visualized part of the data
  getNameSpace(arr) {
    const nameSpace = [];
    arr.forEach((item) => {
      nameSpace.push(item.data.systemtitle);
      nameSpace.push(item.data.systemnumber);
    });
    this.events.nameSpaceEvent.next(nameSpace);
  }

  // computation damping strength range for the visualized part of the data
  getDampingstrengthRange(arr) {
    const dampingstrengthRange = [null, null];
    arr.forEach((item) => {

      if (dampingstrengthRange[0] !== null && +item.data.dampingstrength) {
        dampingstrengthRange[0] = dampingstrengthRange[0] > +item.data.dampingstrength ? +item.data.dampingstrength : dampingstrengthRange[0];
      } else if (dampingstrengthRange[0] === null && +item.data.dampingstrength) {
        dampingstrengthRange[0] = +item.data.dampingstrength;
      }

      if (dampingstrengthRange[1] !== null && +item.data.dampingstrength) {
        dampingstrengthRange[1] = dampingstrengthRange[1] < +item.data.dampingstrength ? +item.data.dampingstrength : dampingstrengthRange[1];
      } else if (dampingstrengthRange[1] === null && +item.data.dampingstrength) {
        dampingstrengthRange[1] = +item.data.dampingstrength;
      }
    });
    this.events.dampingstrengthRangeEvent.next(dampingstrengthRange);
  }

  // select all fire resistance values for filter
  getFireResistanceFilters(arr) {
    const fireResistanceFilters = new Set();
    arr.forEach((item) => {
      fireResistanceFilters.add(item.data.fireresistance);
    });
    this.events.fireResistanceFiltersEvent.next(fireResistanceFilters);
  }

}
