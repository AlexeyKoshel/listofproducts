import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class EventsService {
  allProductsArrayEvent: Subject<any>;
  filteredProductsEvent: Subject<any>;
  nameSpaceEvent: Subject<any>;
  filterParameterEvent: Subject<any>;
  dampingstrengthRangeEvent: Subject<any>;
  watchlistItemEvent: Subject<any>;
  watchlistArrayEvent: Subject<any>;
  updateFiltersParametersEvent: Subject<any>;
  fireResistanceFiltersEvent: Subject<any>;


  constructor() {
    this.allProductsArrayEvent = new Subject<any>();
    this.filteredProductsEvent = new Subject<any>();
    this.nameSpaceEvent = new Subject<any>();
    this.filterParameterEvent = new Subject<any>();
    this.dampingstrengthRangeEvent = new Subject<any>();
    this.watchlistItemEvent = new Subject<any>();
    this.watchlistArrayEvent = new Subject<any>();
    this.updateFiltersParametersEvent = new Subject<any>();
    this.fireResistanceFiltersEvent = new Subject<any>();
  }

}
