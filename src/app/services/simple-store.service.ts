import { Injectable } from '@angular/core';
import {EventsService} from './events.service';

@Injectable()
export class SimpleStoreService {
  allProductsArray: Array<any>;
  filteredProducts: Array<any>;
  watchlistArray: Array<any>;

  constructor(private events: EventsService) {
    this.allProductsArray = [];
    this.filteredProducts = [];
    this.watchlistArray = [];

    this.events.allProductsArrayEvent.subscribe((data) => {
      this.allProductsArray = data;
      this.filteredProducts = this.allProductsArray;
    });
    this.events.filteredProductsEvent.subscribe((products) => {
      this.filteredProducts = products;
    });
    this.events.watchlistArrayEvent.subscribe((nweWatchlistArray) => {
      this.watchlistArray = nweWatchlistArray;
    });

  }




}
