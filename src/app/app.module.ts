import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
  MatInputModule,
  MatTooltipModule,
  MatButtonModule,
  MatTabsModule,
  MatCardModule,
  MatExpansionModule,
  MatCheckboxModule,
  MatDialogModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatButtonToggleModule,
  MatListModule, MatProgressSpinnerModule,
} from '@angular/material';
import { GestureConfig } from '@angular/material';

import {ProductsService} from './services/products.service';
import {EventsService} from './services/events.service';
import {SimpleStoreService} from './services/simple-store.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductsComponent } from './components/products/products.component';
import { WatchlistComponent } from './components/watchlist/watchlist.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { GalleryDialogComponent } from './components/product-card/gallery-dialog/gallery-dialog.component';
import { ProductListComponent } from './components/product-list/product-list.component';


const appRoutes: Routes = [
  { path: 'products', component: ProductsComponent},
  { path: 'watchlist', component: WatchlistComponent},
  { path: '**', redirectTo: 'products' }
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    ProductsComponent,
    WatchlistComponent,
    ProductCardComponent,
    GalleryDialogComponent,
    ProductListComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    LazyLoadImageModule,
    InfiniteScrollModule,
    MatTabsModule,
    MatTooltipModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatListModule,
    MatProgressSpinnerModule,
  ],
  entryComponents: [
    GalleryDialogComponent,
  ],
  providers: [
    EventsService,
    SimpleStoreService,
    ProductsService,
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  private static matButton: any;
}
